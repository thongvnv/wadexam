﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WADExam.Models;

namespace WADExam.Data
{
    public class DataInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<WADExamContext>
    {
        protected override void Seed(WADExamContext context)
        {
            var products = new List<Product>
            {
                new Product{ProductName="quan", SupplierID=1, CategoryID=1, QuantityPerUnit="1", UnitPrice="1", UnitslnStock="1", UnitOnOrder="1", ReorderLevel="1", Discontinued="1"},
                new Product{ProductName="quan", SupplierID=1, CategoryID=1, QuantityPerUnit="1", UnitPrice="1", UnitslnStock="1", UnitOnOrder="1", ReorderLevel="1", Discontinued="1"},
                new Product{ProductName="quan", SupplierID=1, CategoryID=1, QuantityPerUnit="1", UnitPrice="1", UnitslnStock="1", UnitOnOrder="1", ReorderLevel="1", Discontinued="1"},
                new Product{ProductName="quan", SupplierID=1, CategoryID=1, QuantityPerUnit="1", UnitPrice="1", UnitslnStock="1", UnitOnOrder="1", ReorderLevel="1", Discontinued="1"},
            };
            products.ForEach(s => context.Products.Add(s));
            context.SaveChanges();

            var categorys = new List<Category>
            {
            new Category{CategoryName = "Ao", Description = "Ao dai", Picture = "aa"},
            new Category{CategoryName = "Ao 1", Description = "Ao dai 1", Picture = "aa2"},
            new Category{CategoryName = "Ao 2", Description = "Ao dai 2", Picture = "aa3"},
            };
            categorys.ForEach(s => context.Categories.Add(s));
            context.SaveChanges();
        }
    }
}
