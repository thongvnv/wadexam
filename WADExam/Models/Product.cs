﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WADExam.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int SupplierID { get; set; }
        public int CategoryID { get; set; }
        public string QuantityPerUnit { get; set; }
        public string UnitPrice { get; set; }
        public string UnitslnStock { get; set; }
        public string UnitOnOrder { get; set; }
        public string ReorderLevel { get; set; }
        public string Discontinued { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}
